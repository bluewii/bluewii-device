#include <stdlib.h>
#include <string.h>

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3/stm32/i2c.h>
#include <libopencm3/usb/dwc/otg_fs.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/hid.h>


////////////////////////
//
//  Controller
//

/*
 * start without encryption:
 * START, 0xF0, 0x55, STOP START, 0xFB, 0x00, STOP
 * read ID
 * START, 0xFA, STOP READ 6 bytes
 * read measurements
 * START, 0x00, STOP READ 6 bytes
 * byte 0 : X axis 
 * byte 1 : Y axis 
 * byte 3 : acc X [9:2]
 * byte 4 : acc Y [9:2]
 * byte 5 : acc Z [9:2]
 * byte 6 : acc Z [0:1], acc Y [3:2], acc Z [5:4], C, Z
 */

#define CTRLR_COMMUNICATION

#define WII_NUNCHUK_ADDR          0x52
#define WII_NUNCHUK_FREQ            100
#define WII_NUNCHUK_DATA_LENGTH     6
#define WII_NUNCHUK_C_BTN_MASK    0b00000010
#define WII_NUNCHUK_Z_BTN_MASK    0b00000001
#ifdef CTRLR_COMMUNICATION
static uint8_t WII_NUNCHUK_ID[] = {0x00, 0x00, 0xA4, 0x20};
#endif

enum Controller_state_e {
  CTRLR_UNINITILIZED = 0,
  CTRLR_INITILIZED = 1,
  CTRLR_PRESENT = 2,
};

typedef struct _Controller_t {
  enum Controller_state_e state;
  uint8_t buf_in[8];
  uint8_t buf_out[8];
  uint8_t stick_X;
  uint8_t stick_Y;
  uint8_t buttons;
} Controller_t;

#ifdef CTRLR_COMMUNICATION
static Controller_t controller;
#endif

//
//  Controller - end
//
////////////////////////

////////////////////////
//
//  USB
//

enum Usb_state_e {
  USB_UNINITILIZED = 0,
  USB_INITILIZED = 1,
};

enum Usb_state_e usb_state;

static usbd_device *usbd_dev;

const struct usb_device_descriptor dev_descr = {
  .bLength = USB_DT_DEVICE_SIZE,
  .bDescriptorType = USB_DT_DEVICE,
  .bcdUSB = 0x0200,
  .bDeviceClass = 0,       // taken from the interface
  .bDeviceSubClass = 0,    // taken from the interface
  .bDeviceProtocol = 0,    // taken from the interface
  .bMaxPacketSize0 = 64,   // either 8, 16, 32, 64
  .idVendor = 0x0079,
  .idProduct = 0x0011,
  .bcdDevice = 0x0200,
  .iManufacturer = 1,
  .iProduct = 2,
  .iSerialNumber = 3,
  .bNumConfigurations = 1,
};

static const uint8_t hid_report_descriptor[] = {
  0x05, 0x01, /* USAGE_PAGE (Generic Desktop)         */
  0x09, 0x05, /* USAGE (Game Pad)                     */
  0xa1, 0x01, /* COLLECTION (Application)             */
  0xa1, 0x00, /*   COLLECTION (Physical)              */
  0x05, 0x09, /*     USAGE_PAGE (Button)              */
  0x19, 0x01, /*     USAGE_MINIMUM (Button 1)         */
  0x29, 0x02, /*     USAGE_MAXIMUM (Button 2)         */
  0x15, 0x00, /*     LOGICAL_MINIMUM (0)              */
  0x25, 0x01, /*     LOGICAL_MAXIMUM (1)              */
  0x95, 0x02, /*     REPORT_COUNT (2)                 */
  0x75, 0x01, /*     REPORT_SIZE (1)                  */
  0x81, 0x02, /*     INPUT (Data,Var,Abs)             */
  0x95, 0x01, /*     REPORT_COUNT (1)                 */
  0x75, 0x06, /*     REPORT_SIZE (6)                  */
  0x81, 0x01, /*     INPUT (Cnst,Ary,Abs)             */
  0x05, 0x01, /*     USAGE_PAGE (Generic Desktop)     */
  0x09, 0x30, /*     USAGE (X)                        */
  0x09, 0x31, /*     USAGE (Y)                        */
  0x15, 0x81, /*     LOGICAL_MINIMUM (-127)           */
  0x25, 0x7f, /*     LOGICAL_MAXIMUM (127)            */
  0x75, 0x08, /*     REPORT_SIZE (8)                  */
  0x95, 0x02, /*     REPORT_COUNT (2)                 */
  0x81, 0x06, /*     INPUT (Data,Var,Rel)             */
  0xc0,       /*   END_COLLECTION                     */
  0xc0        /* END_COLLECTION                       */
};

// Create an anonymous struct and create a hid_function variable
// out of that and initilize it with a default values.
static const struct {
  struct usb_hid_descriptor hid_descriptor;
  struct {
    uint8_t bReportDescriptorType;
    uint16_t wDescriptorLength;
  } __attribute__((packed)) hid_report;
} __attribute__((packed)) hid_function =
{
  .hid_descriptor = {
    .bLength = sizeof(hid_function),
    .bDescriptorType = USB_DT_HID,
    .bcdHID = 0x0100,
    .bCountryCode = 0,
    .bNumDescriptors = 1,
  },

  .hid_report = {
    .bReportDescriptorType = USB_DT_REPORT,
    .wDescriptorLength = sizeof(hid_report_descriptor),
  }
};

const struct usb_endpoint_descriptor hid_endpoint = {
  .bLength = USB_DT_ENDPOINT_SIZE,
  .bDescriptorType = USB_DT_ENDPOINT,
  .bEndpointAddress = 0x81,   // bit 7 is direction 0 = OUT, 1 = IN
  .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
  .wMaxPacketSize = 8,
  .bInterval = 0x04,   // polling interval in miliseconds
  .extra = (void*)0,
  .extralen = 0,
};

const struct usb_interface_descriptor hid_iface = {
  .bLength = USB_DT_INTERFACE_SIZE,
  .bDescriptorType = USB_DT_INTERFACE,
  .bInterfaceNumber = 0,
  .bAlternateSetting = 0,
  .bNumEndpoints = 1,
  .bInterfaceClass = USB_CLASS_HID,
  .bInterfaceSubClass = 0,
  .bInterfaceProtocol = 0,
  .iInterface = 0,

  .endpoint = &hid_endpoint,

  .extra = &hid_function,
  .extralen = sizeof(hid_function),
};

const struct usb_interface ifaces[] = {{
  .num_altsetting = 1,
  .altsetting = &hid_iface,
}};

const struct usb_config_descriptor config = {
  .bLength = USB_DT_CONFIGURATION_SIZE,
  .bDescriptorType = USB_DT_CONFIGURATION,
  .wTotalLength = 0,
  .bNumInterfaces = 1,
  .bConfigurationValue = 1,
  .iConfiguration = 0,
  .bmAttributes = 0xC0,
  .bMaxPower = 0x32,

  .interface = ifaces,
};

static const char *usb_strings[] = {
  "Black Sphere Technologies",
  "HID Demo",
  "GamePad",
};

/* Buffer used for control requests. */
static uint8_t usbd_control_buffer[128];

static enum usbd_request_return_codes
hid_control_request(usbd_device *dev,
                    struct usb_setup_data *req,
                    uint8_t **buf, uint16_t *len,
                    void (**complete)(usbd_device *dev, struct usb_setup_data *req))
{
  (void)complete;
  (void)dev;

  if((req->bmRequestType != 0x81) ||
     (req->bRequest != USB_REQ_GET_DESCRIPTOR) ||
     (req->wValue != 0x2200))
    return USBD_REQ_NOTSUPP;

  /* Handle the HID report descriptor. */
  *buf = (uint8_t *)hid_report_descriptor;
  *len = sizeof(hid_report_descriptor);

  systick_counter_enable();
  usb_state = USB_INITILIZED;
  return USBD_REQ_HANDLED;
}

static void hid_set_config(usbd_device *dev, uint16_t wValue)
{
  (void)wValue;
  (void)dev;

  usbd_ep_setup(dev, 0x81, USB_ENDPOINT_ATTR_INTERRUPT, 4, NULL);

  usbd_register_control_callback(
        dev,
        USB_REQ_TYPE_STANDARD | USB_REQ_TYPE_INTERFACE,
        USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
        hid_control_request);
}

//
//  USB - end
//
////////////////////////

static void setup_clock(void)
{
  // usbd.h says 'It is required that the 48MHz USB clock is already available.'
  rcc_clock_setup_in_hse_8mhz_out_72mhz();

  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_GPIOC);
  rcc_periph_clock_enable(RCC_SPI2);
  rcc_periph_clock_enable(RCC_I2C1);
  rcc_periph_clock_enable(RCC_OTGFS);

  systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);
  // SysTick interrupt every N clock pulses: set reload to N-1
  // 72MHz / 8 = 9MHz
  // 9000000Hz / 90000 = 100Hz
  systick_set_reload(89999);
  systick_interrupt_enable();
}

static void setup_gpio(void)
{
  gpio_set(GPIOC, GPIO13);
  gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
}

static void setup_i2c(void)
{
  // STM32F1 uses i2c_common_v1.h

  i2c_reset(I2C1);
  i2c_peripheral_disable(I2C1);
  // TODO(michalc): review argument 2 and 3
  // gpio_set_mode not gpio_mode_setup because it's F1
  // I2C1 is a default alternate function for those pins
  gpio_set_mode(GPIOB,
                GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN,
                GPIO6 | GPIO7);

  i2c_set_speed(I2C1, i2c_speed_sm_100k, 8);
  i2c_peripheral_enable(I2C1);
}

int main(void)
{
  setup_clock();
  setup_gpio();

#ifdef CTRLR_COMMUNICATION
  setup_i2c();
#endif

  ////////////////////////
  //
  //  USB (PA12 - D+, PA11 - D-)
  //

  /*
   * This is a somewhat common cheap hack to trigger device re-enumeration
   * on startup.  Assuming a fixed external pullup on D+, (For USB-FS)
   * setting the pin to output, and driving it explicitly low effectively
   * "removes" the pullup.  The subsequent USB init will "take over" the
   * pin, and it will appear as a proper pullup to the host.
   * The magic delay is somewhat arbitrary, no guarantees on USBIF
   * compliance here, but "it works" in most places.
   */
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO12);
  gpio_clear(GPIOA, GPIO12);
  // Delay the enumeration - this works.
  for (uint32_t i = 0; i < 0x800000; i++)
  {
    __asm__("nop");
  }

  usbd_dev = usbd_init(&st_usbfs_v1_usb_driver,
                       &dev_descr,
                       &config, usb_strings, sizeof(usb_strings)/sizeof(char *), usbd_control_buffer,
                       sizeof(usbd_control_buffer));
  usbd_register_set_config_callback(usbd_dev, hid_set_config);

  //
  //  USB - end
  //
  ////////////////////////

  while (1)
  {
    // TODO(michalc): some sleep would be nice.
    usbd_poll(usbd_dev);
  }

}

// systick counter gets enabled in the hid_control_request().
// That's because polling the controller makes sense if USB works well.
void sys_tick_handler(void)
{
  uint8_t buf[3] = {0, 0, 0};
#ifdef CTRLR_COMMUNICATION
  buf[0] = controller.stick_X;
  buf[1] = controller.stick_Y;
  buf[2] = controller.buttons;
#endif

  usbd_ep_write_packet(usbd_dev, 0x81, buf, 3);

#ifdef CTRLR_COMMUNICATION
  if (controller.state == CTRLR_UNINITILIZED)
  {
    // Init without encryption.
    controller.buf_out[0] = 0xF0;
    controller.buf_out[1] = 0x55;
    i2c_transfer7(I2C1, WII_NUNCHUK_ADDR, controller.buf_out, 2, NULL, 0);
    controller.buf_out[0] = 0xFB;
    controller.buf_out[1] = 0x00;
    i2c_transfer7(I2C1, WII_NUNCHUK_ADDR, controller.buf_out, 2, NULL, 0);
    controller.state = CTRLR_INITILIZED;
  }
  if (controller.state == CTRLR_INITILIZED)
  {
    controller.buf_out[0] = 0xFA;
    i2c_transfer7(I2C1, WII_NUNCHUK_ADDR, controller.buf_out, 1, NULL, 0);
    i2c_transfer7(I2C1, WII_NUNCHUK_ADDR, NULL, 0, controller.buf_in, 4);
    
    if (memcmp(controller.buf_in, WII_NUNCHUK_ID, 4) == 0)
      controller.state = CTRLR_PRESENT;
  }
  if (controller.state == CTRLR_PRESENT)
  {
    controller.buf_out[0] = 0x0;
    i2c_transfer7(I2C1, WII_NUNCHUK_ADDR, controller.buf_out, 1, NULL, 0);
    i2c_transfer7(I2C1, WII_NUNCHUK_ADDR, NULL, 0, controller.buf_in, 6);
    controller.stick_X = controller.buf_in[0];
    controller.stick_Y = controller.buf_in[1];
    controller.buttons = controller.buf_in[5] &
      (WII_NUNCHUK_C_BTN_MASK | WII_NUNCHUK_Z_BTN_MASK);

    if (controller.buttons & WII_NUNCHUK_C_BTN_MASK)
    {
      gpio_clear(GPIOC, GPIO13);
    }
    else
    {
      gpio_set(GPIOC, GPIO13);
    }
  }
#endif
}
