# BlueWii device

Wii Nunchuk converted to USB with Blue pill (STM32F103C8).

Based on **libopencm3**.

The main goal is to get a working USB device for future projects
but also test a controller for Blender, Zbrush, etc.

```
.
├── libopencm3
│   ├── doc
│   ├── include
│   ├── ld
│   ├── lib
│   ├── mk
│   ├── scripts
│   └── tests
├── src           // source files
└── Makefile      // Makefile for getting/building libopencm3
```


## Development

### Flashing/debugging

When using ST-Link programmer:

```
st-flash write binary.bin 0x08000000
```

I've been testing **Black Magic Probe** during development. You connect to it with:

```
~/gcc-arm-none-eabi-7-2018-q2-update/bin/arm-none-eabi-gdb-py \
  -ex "target extended-remote /dev/ttyACM0"  \
  -ex "monitor scan_swdp"  \
  -ex "attach 1"  \
  bluewii.elf
```

#### Flash with **OpenOCD**

Start **OpenOCD** server:

```sh
openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg
```

Connect to the server (port 4444 is for CLI commands):

```sh
telnet localhost 4444
```

Load the binary (when using **OpenOCD** use `elf` file):

```
reset halt
flash write_image erase binary.elf
reset
```

#### Flash via **GBD**

```sh
arm-none-eabi-gdb binary.elf
```

Port 3333 is for `gdb`:

```
target extended-remote : 3333
load
run
```

