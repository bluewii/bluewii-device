ifneq ($(V), 1)
MFLAGS += --no-print-dir
Q := @
endif

all:
	$(Q)if [ ! -f libopencm3/Makefile ]; then \
		echo "Initialising git submodules..." ;\
		git submodule init ;\
		git submodule update ;\
	fi
	#
	# Building libopencm3 lib!
	#
	$(Q)$(MAKE) $(MFLAGS) -C libopencm3 V=0 lib
	$(Q)$(MAKE) $(MFLAGS) -C src
	# Verify the size of the resulting binary.
	$(Q)$(MAKE) $(MFLAGS) -C src size

clean:
	$(Q)$(MAKE) $(MFLAGS) -C src $@
	
clean_all:
	$(Q)$(MAKE) $(MFLAGS) -C libopencm3 $@
	$(Q)$(MAKE) $(MFLAGS) -C src $@
